from django.contrib import admin

# Register your models here.
from recipes.models import Recipe, Step, Measure, FoodItem, Ingredient
admin.site.register(Recipe)
admin.site.register(Step)
admin.site.register(Measure)
admin.site.register(FoodItem)
admin.site.register(Ingredient)
